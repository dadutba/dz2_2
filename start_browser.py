#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Данный скрипт пытается открыть доступный (желательно по умолчанию)
браузер для данной платформы

"""
if __name__ == '__main__':

#################################################
# Простой вариант, плохо работает на windows10:
#################################################
    #import webbrowser
    #webbrowser.open_new('http://google.ru')

###########################################################
# Варианты сложнее. Тестировалось только на Windows 10
# По возможности запускается только браузер (без
# передачи ему url)
###########################################################
    
    import os
    import subprocess
    import sys
    
    def try_execute(command, arg='', report=False):
        """Попытаться выполнить команду с указанными аргументами
        
        Аргументы:
            command: команда
            arg:     аргументы для команды
            report:  Если True - вывести сообщение об ошибке выполнения,
                     при наличии таковой
        
        Возвращаемое значение:
            True - если команда выполнена успешно, иначе - False
            
        """
        try:
            cmd = '"{}" {}'.format(command, arg)
            result = subprocess.run(cmd,
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE)
            
            if result.returncode != 0:
                if result.returncode < 0:
                    print ('Child was terminated by signal {}'
                           .format(-result.returncode))
                else:
                    print ('Child returned {}'.format(result.returncode))
                print('command: {}'.format(cmd))
                print('stdout: {}'.format(result.stdout.decode('utf-8')))
                print('stderr: {}'.format(result.stderr.decode('utf-8')))
            return True
        except OSError as e:
            if report:
                print ('Execution failed: {}'.format(e))
            return False
    
    if sys.platform.startswith('darwin'): # OSX
        
        try_execute('Safari', report=True)
        
#        from Foundation import CFPreferencesCopyAppValue
#        
#        bundle_identifier = ''
#    
#        handlers = CFPreferencesCopyAppValue('LSHandlers',
#                                             'com.apple.LaunchServices')
#        
#        try:
#            handler = next(x for x in handlers if x.get('LSHandlerURLScheme')
#                           == 'http')
#            bundle_identifier = handler.get('LSHandlerRoleAll')
#        except StopIteration:
#            pass
#        
#        try:
#            result = subprocess.run('open', '-b {}'.format(bundle_identifier),
#                                    stdout=subprocess.PIPE,
#                                    stderr=subprocess.PIPE)
#            
#            if result.returncode != 0:
#                if result.returncode < 0:
#                    print ('Child was terminated by signal {}'
#                           .format(-result.returncode))
#                else:
#                    print ('Child returned {}'.format(result.returncode))
#                print('stdout: {}'.format(result.stdout.decode('utf-8')))
#                print('stderr: {}'.format(result.stderr.decode('utf-8')))
#        except OSError as e:
#            print ('Execution failed: {}'.format(e))
            
    elif os.name == 'nt': # Windows
        
        from winreg import HKEY_CLASSES_ROOT, HKEY_CURRENT_USER
        from winreg import HKEY_LOCAL_MACHINE
        from winreg import OpenKey, QueryValueEx
        
        try:
            with OpenKey(HKEY_CURRENT_USER,
                         r'Software\Microsoft\Windows\Shell\Associations\URLAssociations\http\UserChoice') as key:
                cmd = QueryValueEx(key, 'ProgId')[0]
            with OpenKey(HKEY_CLASSES_ROOT,
                         cmd + r'\shell\open\command') as key:
                cmd = QueryValueEx(key, None)[0]
        except FileNotFoundError:
            try:
                with OpenKey(HKEY_CURRENT_USER,
                             r'Software\Clients\StartMenuInternet') as key:
                    cmd = QueryValueEx(key, None)[0]
            except FileNotFoundError:
                try:
                    with OpenKey(HKEY_LOCAL_MACHINE,
                                 r'Software\Clients\StartMenuInternet') as key:
                        cmd = QueryValueEx(key, None)[0]
                except FileNotFoundError:
                    cmd = 'iexplore.exe'
        if len(cmd) and cmd[0] == '"':
            prev = '"'
            i = 1
            while i < len(cmd):
                cur = cmd[i]
                if cur == '"':
                    if prev != '\\':
                        cmd = cmd[1:i]
                        break
                prev = cur
                i += 1
            if cmd == '"':
                cmd = ''
        else:
            cmd = cmd.split()
            if not len(cmd):
                cmd = ''
            else:
                cmd = cmd[0]
                
        os.startfile('"{}"'.format(cmd))
        
    else: # os.name == 'posix' # Linux, Unix кроме OSX
        
        try_list = [('sensible-browser',),
                    ('x-www-browser',),
                    ('gnome-www-browser',),
                    ('xdg-open', 'http:ya.ru'),
                    ('gnome-open', 'http:ya.ru', True)]
        for entry in try_list:
            if try_execute(*entry): break

    
    