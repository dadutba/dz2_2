#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Скрипт конвертирует все картинки .jpg
из папки Converter/Source
в папку Converter/Dest,
делая их шириной 200px

"""
if __name__ == '__main__':
    
    import os
    import subprocess
    import sys
    
    source_path = os.path.join('Converter', 'Source')
    dest_path = os.path.join('Converter', 'Dest')
    exe_path = 'Converter'
    
    script_dir = os.path.dirname(os.path.abspath(__file__))
    source_path = os.path.join(script_dir, source_path)
    dest_path = os.path.join(script_dir, dest_path)
    
    if sys.platform.startswith('darwin'): # OSX
        exe_path = 'sips'
    elif os.name == 'nt': # Windows
        exe_path = os.path.join(script_dir, exe_path, 'convert.exe')
    else:
        exe_path = 'convert'
        
    def try_execute(command, arg='', report=False):
        """Попытаться выполнить команду с указанными аргументами
        
        Аргументы:
            command: команда
            arg:     аргументы для команды
            report:  Если True - вывести сообщение об ошибке выполнения,
                     при наличии таковой
        
        Возвращаемое значение:
            True - если команда выполнена успешно, иначе - False
            
        """
        try:
            cmd = '"{}" {}'.format(command, arg)
            result = subprocess.run(cmd,
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE)
            
            if result.returncode != 0:
                if result.returncode < 0:
                    print ('Child was terminated by signal {}'
                           .format(-result.returncode))
                else:
                    print ('Child returned {}'.format(result.returncode))
                print('command: {}'.format(cmd))
                print('stdout: {}'.format(result.stdout.decode('utf-8')))
                print('stderr: {}'.format(result.stderr.decode('utf-8')))
            return True
        except OSError as e:
            if report:
                print ('Execution failed: {}'.format(e))
            return False
        
    def prepare_jpeg_files(source_path, dest_path):
        """Подготовка имен входных и выходных jpg файлов для циклов
        
        Аргументы:
            source_path: каталог с исходными файлами jpg
            dest_path:   каталог для размещения выходных файлов
            
        Возвращаемое значение:
            Генератор кортежей входной-выходной файл
            
        """
        files = [file for file in os.listdir(source_path)
                 if file.lower().endswith('.jpg')
                 or file.lower().endswith('.jpeg')]
        return (('"{}"'.format(os.path.join(source_path, file)),
                 '"{}"'.format(os.path.join(dest_path, file)))
                 for file in files)
        
    if sys.platform.startswith('darwin'): # OSX
        try:
            if not os.path.exists(dest_path):
                os.makedirs(dest_path)
        except OSError:
            sys.exit(1)
        for infile, outfile in prepare_jpeg_files(source_path, dest_path):
            if not try_execute('cp', '{} {}'.format(infile, outfile), True):
                sys.exit(3)
            if not try_execute(exe_path,
                               ' --resampleWidth 200 {}'.format(outfile),
                               True):
                sys.exit(2)
    else: # Кроме OSX
        try:
            if not os.path.exists(dest_path):
                os.makedirs(dest_path)
        except OSError:
            sys.exit(1)
        
        for infile, outfile in prepare_jpeg_files(source_path, dest_path):
            if not try_execute(exe_path,
                               '{} -resize 200 {}'
                               .format(infile, outfile),
                               True):
                sys.exit(2)
                        