#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Задание
# мне нужно отыскать файл среди десятков других
# я знаю некоторые части этого файла (на память или из другого источника)
# я ищу только среди .sql файлов
# 1. программа ожидает строку, которую будет искать (input())
# после того, как строка введена, программа ищет её во всех файлах
# выводит список найденных файлов построчно
# выводит количество найденных файлов
# 2. снова ожидает ввод
# поиск происходит только среди найденных на этапе 1
# 3. снова ожидает ввод
# ...
# Выход из программы программировать не нужно.
# Достаточно принудительно остановить, для этого можете нажать Ctrl + C

# Пример на настоящих данных

# python3 find_procedure.py
# Введите строку: INSERT
# ... большой список файлов ...
# Всего: 301
# Введите строку: APPLICATION_SETUP
# ... большой список файлов ...
# Всего: 26
# Введите строку: A400M
# ... большой список файлов ...
# Всего: 17
# Введите строку: 0.0
# Migrations/000_PSE_Application_setup.sql
# Migrations/100_1-32_PSE_Application_setup.sql
# Всего: 2
# Введите строку: 2.0
# Migrations/000_PSE_Application_setup.sql
# Всего: 1

# не забываем организовывать собственный код в функции

import os

migrations = 'Migrations'
script_dir = os.path.dirname(os.path.abspath(__file__))
migrations = os.path.join(script_dir, migrations)

if __name__ == '__main__':
    files = [file for file in os.listdir(migrations)
             if file.lower().endswith('.sql')]
    print('Поиск в {} sql файлах'.format(len(files)))
    while True:
        search_template = input('Введите строку для поиска:').lower()
        iterable, files = (files, [])
        for file in iterable:
            with open(os.path.join(migrations, file)) as f:
                if search_template in f.read().lower():
                    # sql файлы обычно не очень большие,
                    # будем загружать в память.
                    files.append(file)
                    print(file)
        count = len(files)
        print('Найдено файлов:', count)
        if count < 2: break
        

